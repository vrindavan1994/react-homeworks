import React from 'react';
import './App.scss'
import Header from "./containers/Header/Header";
import Footer from "./containers/Footer/Footer";
import PropTypes from "prop-types";
import AppRoutes from "./containers/AppRoutes/AppRoutes";

const App = () => {
    return (
        <div className="app">
            <Header/>
            <AppRoutes/>
            <Footer/>
        </div>
    );
}


export default App;

App.propTypes = {
    modal: PropTypes.bool,
    toggleModal: PropTypes.func,
}
