import {ADD_TO_CART, REMOVE_FROM_CART, CLEAN_OUT_CART} from "../actionTypes";

export const addItemToCart = item => {
    return {type: ADD_TO_CART, payload: {item}};
}

export const removeCartItem = vendorCode => {
    return {type: REMOVE_FROM_CART, payload: {vendorCode}};
}

export const cleanOutCart = () => {
    return {type: CLEAN_OUT_CART, payload: []}
}

