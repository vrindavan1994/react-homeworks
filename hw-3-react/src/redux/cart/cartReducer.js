import {ADD_TO_CART, REMOVE_FROM_CART, CLEAN_OUT_CART} from "../actionTypes";

const initialState = [];

export const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            return [...state, action.payload.item]
        case REMOVE_FROM_CART:
            return state.filter(book => book.vendorCode !== action.payload.vendorCode)
        case CLEAN_OUT_CART:
            return initialState
        default:
            return state;
    }
}
