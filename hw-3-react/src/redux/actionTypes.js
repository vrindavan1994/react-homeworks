export const FETCH_BOOKS = 'FETCH_BOOKS';

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CLEAN_OUT_CART = 'CLEAN_OUT_CART';

export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES';
export const REMOVE_FROM_FAVORITES = 'REMOVE_FROM_FAVORITES';

export const SHOW_CART_ADD_MODAL = 'SHOW_CART_ADD_MODAL'
export const SHOW_CART_REMOVE_MODAL = 'SHOW_CART_ADD_MODAL'
export const HIDE_MODAL = 'HIDE_MODAL'


