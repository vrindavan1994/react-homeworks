import {combineReducers} from "redux";
import {booksReducer} from './books/booksReducer';
import {cartReducer} from './cart/cartReducer';
import {favoritesReducer} from "./favorites/favoritesReducer";
import {modalReducer} from "./modal/modalReducer";

export const rootReducer = combineReducers({
    books: booksReducer,
    cart: cartReducer,
    favorites: favoritesReducer,
    modal: modalReducer,
})