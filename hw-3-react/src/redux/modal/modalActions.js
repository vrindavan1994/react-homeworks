import {SHOW_CART_ADD_MODAL,HIDE_MODAL,SHOW_CART_REMOVE_MODAL} from "../actionTypes";

export function showAddCartModal(){
    return {type: SHOW_CART_ADD_MODAL}
}
export function hideCartModal(){
    return {type: HIDE_MODAL}
}
export function showRemoveCartModal(){
    return {type: SHOW_CART_REMOVE_MODAL}
}
