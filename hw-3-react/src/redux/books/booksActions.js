import axios from 'axios'
import {FETCH_BOOKS} from "../actionTypes";

export function fetchBooks() {
    return dispatch => {
        axios('./books-storage.json')
            .then(r => {
                dispatch({
                    type: FETCH_BOOKS,
                    payload: r.data
                });

            });
    }
}