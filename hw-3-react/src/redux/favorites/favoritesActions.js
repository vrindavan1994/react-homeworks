import {ADD_TO_FAVORITES, REMOVE_FROM_FAVORITES} from '../actionTypes'


export const addFavorite = item => {
    return { type: ADD_TO_FAVORITES, payload: { item } }
}

export const removeFavorite = vendorCode => {
    return {type: REMOVE_FROM_FAVORITES, payload: { vendorCode }}
}
