import {ADD_TO_FAVORITES, REMOVE_FROM_FAVORITES} from "../actionTypes";

const initialState = [];

export const favoritesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_FAVORITES:
            return [...state, action.payload.item]
        case REMOVE_FROM_FAVORITES:
            return state.filter(book => book.vendorCode !== action.payload.vendorCode)
        default:
            return state
    }
}