import React from 'react';
import './Favorites.scss';
import BookTemplate from "../../components/BookTemplate/BookTemplate";
import {removeFavorite} from "../../redux/favorites/favoritesActions";
import {useDispatch, useSelector} from "react-redux";


const Favorites = () => {
    const dispatch = useDispatch();
    const favoritesList = useSelector(state => {
        return state.favorites
    });

    return (
        <div className='favorites'>
            {!!favoritesList.length ? (
                favoritesList.map((book, index) => (
                        <BookTemplate
                            header={
                                <span className={"book-container__favorites-star"}
                                      onClick={() => dispatch(removeFavorite(book.vendorCode))}>
                                <i className="fas fa-star"/>
                            </span>
                            }
                            title={book.title}
                            vendorCode={book.vendorCode}
                            color={book.color}
                            path={book.path}
                            price={book.price}
                            key={index}
                        />
                    )
                )
            ) : (
                <p className='cart__no-items-text'>Oops... No books have been added</p>
            )}

        </div>
    );
};

export default Favorites;