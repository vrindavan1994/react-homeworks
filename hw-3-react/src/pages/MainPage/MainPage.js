import React, {useEffect} from 'react';
import Book from "./Book/Book";
import "./MainPage.scss"
import PropTypes from 'prop-types'
import {useDispatch, useSelector} from 'react-redux'
import {fetchBooks} from "../../redux/books/booksActions";
import Modal from "../../components/Modal/Modal";
import {hideCartModal} from "../../redux/modal/modalActions";

const MainPage = () => {

    const dispatch = useDispatch()
    const fetchedBooks = useSelector(state => {
        return state.books.all
    });
    const modalState = useSelector(state => {
        return state.modal.isAddToCart
    });


    useEffect(() => dispatch(fetchBooks()), [dispatch])


    const books = fetchedBooks.map((el, index) =>
        <Book
            title={el.title}
            price={el.price}
            path={el.path}
            vendorCode={el.vendorCode}
            color={el.color}
            key={index}
            item={el}
        />)

    return (
        <div className="main-page">
            {books}
            {modalState ?
                <Modal
                    header="Excellent choice, pal!"
                    text="Do you wanna add this book to your cart?"
                    closeButton={() => dispatch(hideCartModal())}
                    actions={
                        <>
                            <button onClick={() => dispatch(hideCartModal())}
                                    className="modal__window-footer-btn-cancel">CANCEL
                            </button>
                            <button onClick={() =>
                                dispatch(hideCartModal())}
                                    className="modal__window-footer-btn-ok">ADD
                                TO CART
                            </button>
                        </>
                    } onBlur={(event) => event.currentTarget === event.target && hideCartModal()}
                /> : null
            }
        </div>
    );

};


export default MainPage;

MainPage.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    key: PropTypes.number,
    items: PropTypes.array
}