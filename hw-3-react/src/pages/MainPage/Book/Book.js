import React from 'react';
import "./Book.scss"
import BookTemplate from "../../../components/BookTemplate/BookTemplate";
import {useDispatch, useSelector} from "react-redux";
import {showAddCartModal} from "../../../redux/modal/modalActions";
import {addFavorite, removeFavorite} from "../../../redux/favorites/favoritesActions";
import {addItemToCart} from "../../../redux/cart/cartActions";

const Book = props => {

    const dispatch = useDispatch()
    const {
        title,
        price,
        path,
        vendorCode,
        color,
        item
    } = props;
    const isFavorite = useSelector(state => !!state.favorites.find(book => book.vendorCode === vendorCode));

    return (
        <>
            <BookTemplate
                title={title}
                price={price}
                path={path}
                color={color}
                vendorCode={vendorCode}
                header={isFavorite ?
                    <span className={"book-container__favorites-star"}
                          onClick={() => dispatch(removeFavorite(vendorCode))}>
        <i className="fas fa-star"/></span>
                    :
                    <span className={"book-container__favorites"}
                          onClick={() => dispatch(addFavorite(item))}>
        <i className="far fa-star"/></span>}
                footer={<button className="book-container__add-to-cart-btn"
                                onClick={() => {
                                    dispatch(showAddCartModal())
                                    dispatch(addItemToCart(item))
                                }}>
                    Add to cart
                </button>}
            />
        </>
    );
}


export default Book;

// Book.propTypes = {
//     title: PropTypes.string,
//     price: PropTypes.string,
//     path: PropTypes.string,
//     vendorCode: PropTypes.string,
//     color: PropTypes.string,
//     favorite: PropTypes.bool
// }