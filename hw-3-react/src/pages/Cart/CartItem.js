import React from 'react';
import BookTemplate from "../../components/BookTemplate/BookTemplate";
import Modal from "../../components/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {hideCartModal, showRemoveCartModal} from "../../redux/modal/modalActions";
import {removeCartItem} from "../../redux/cart/cartActions";


const CartItem = (props) => {

    const dispatch = useDispatch();
    const modalState = useSelector(state => {
        return state.modal.isAddToCart
    });
    const cartItem = useSelector(state => {return state.cart})
    const {
        title,
        price,
        path,
        vendorCode,
        color,
        item
    } = props;

    return (
        <>
            {cartItem ? <BookTemplate
                    deleteButton={<button
                        className='cart__delete-item'
                        onClick={() => dispatch(showRemoveCartModal())}>X</button>}
                    title={title}
                    price={price}
                    color={color}
                    vendorCode={vendorCode}
                    path={path}

                />
                : null}
            {modalState ?
                <Modal
                    header="Are you sure wanna delete this book?!"
                    text="Do not make me laugh,pal, KEEP BUYING"
                    closeButton={() => dispatch(hideCartModal())}
                    actions={
                        <>
                            <button onClick={() => dispatch(hideCartModal())}
                                    className="modal__window-footer-btn-cancel">CANCEL
                            </button>
                            <button onClick={
                                () => {
                                    dispatch(removeCartItem(item))
                                    dispatch(hideCartModal())
                                }}
                                    className="modal__window-footer-btn-ok">DELETE
                            </button>
                        </>
                    } onBlur={(event) => event.currentTarget === event.target && dispatch(hideCartModal())}
                /> : null
            }
        </>
    );
};

export default CartItem;