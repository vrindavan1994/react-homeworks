import React, {useState} from 'react';
import './Cart.scss'
import {useDispatch, useSelector} from "react-redux";
import PurchaseForm from "../../components/PurchaseForm/PurchaseForm";
import BookTemplate from "../../components/BookTemplate/BookTemplate";
import Modal from "../../components/Modal/Modal";
import {showRemoveCartModal, hideCartModal} from "../../redux/modal/modalActions";
import {removeCartItem} from "../../redux/cart/cartActions";


const Cart = () => {

    const dispatch = useDispatch()
    const cartItemsList = useSelector(state => {
        return state.cart
    });
    const modalState = useSelector(state => {
        return state.modal.isAddToCart
    });
    const [removeVendoreCode, setRemoveVendoreCode] = useState(null)


    return (
        <div className='cart-container'>
            {!!cartItemsList.length ? <PurchaseForm/> : null}
            <div className='cart'>
                {!!cartItemsList.length ? (
                    cartItemsList.map((book, index) => (
                        <BookTemplate
                            deleteButton={<button
                                className='cart__delete-item'
                                onClick={() => {
                                    dispatch(showRemoveCartModal())
                                    setRemoveVendoreCode(book.vendorCode)
                                }}>X</button>}
                            title={book.title}
                            vendorCode={book.vendorCode}
                            color={book.color}
                            path={book.path}
                            price={book.price}
                            key={index}
                        />
                    ))
                ) : <p className='cart__no-items-text'>Oops... No books have been added</p>
                }
                {modalState ?
                    <Modal
                        header="Are you sure wanna delete this book?!"
                        text="Do not make me laugh,pal, KEEP BUYING"
                        closeButton={() => dispatch(hideCartModal())}
                        actions={
                            <>
                                <button onClick={() => dispatch(hideCartModal())}
                                        className="modal__window-footer-btn-cancel">CANCEL
                                </button>
                                <button onClick={
                                    () => {
                                        dispatch(removeCartItem(removeVendoreCode))
                                        dispatch(hideCartModal())
                                    }}
                                        className="modal__window-footer-btn-ok">DELETE
                                </button>
                            </>
                        } onBlur={(event) => event.currentTarget === event.target && dispatch(hideCartModal())}
                    /> : null
                }
            </div>
        </div>

    );
};

export default Cart;   