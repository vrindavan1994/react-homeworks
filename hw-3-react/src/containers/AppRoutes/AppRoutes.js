import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Cart from "../../pages/Cart/Cart";
import Favorites from "../../pages/Favorites/Favorites";
import MainPage from "../../pages/MainPage/MainPage";

const AppRoutes = () => {

    return (
        <Switch>
            <Route
                path='/'
                exact
                component={MainPage}/>
            <Route
                path='/Favorites'
                exact
                component={Favorites}
            />
            <Route
                path='/Cart'
                exact
                component={Cart}
            />
        </Switch>
    );
};

export default AppRoutes;

