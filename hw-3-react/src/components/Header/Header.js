import React from 'react';
import {NavLink} from "react-router-dom";
import "./Heaader.scss"

const Header = () => {

        return (
            <header className="header">
                <NavLink className='header__main-link' exact to='/'>
                <h2 className="header__logo">You
                    <span className="header__logo-span">Book</span>
                </h2>
                </NavLink>
                <div className="header__links-container">
                    <NavLink className="header__links-container-link" activeClassName='active' exact to='/'>
                        <i className="fas fa-home"></i>
                        <span>Main</span>
                    </NavLink>
                    <NavLink className="header__links-container-link" activeClassName='active' exact to='/Favorites'>
                        <i className="fas fa-star"></i>
                        <span>Favorites</span>
                    </NavLink>
                    <NavLink className="header__links-container-link" activeClassName='active' exact to='/Cart'>
                        <i className="fas fa-cart-plus"></i>
                        <span>Cart</span>
                    </NavLink>
                </div>
            </header>
        );
    }


export default Header;