import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Cart from "../Cart/Cart";
import Favorites from "../Favorites/Favorites";
import MainPage from "../MainPage/MainPage";

const AppRoutes = (props) => {

    return (
        <Switch>
            <Route
                path='/'
                exact
                component={MainPage}/>
            <Route
                path='/Favorites'
                exact
                component={Favorites}
            />
            <Route
                path='/Cart'
                exact
                component={Cart}
            />
        </Switch>
    );
};

export default AppRoutes;

