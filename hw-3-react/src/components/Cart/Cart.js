import React, {useEffect} from 'react';
import './Cart.scss'
import CartItem from "./CartItem";
import {useDispatch, useSelector} from "react-redux";
import {cartItemLoad} from "../redux/cart/cartActions";
import {getFromLocalStorage} from "../utils";



const Cart = () => {

    const dispatch = useDispatch()
    const cartItems = useSelector( state => {return state.cart.cart})

    useEffect(() => {
        dispatch (cartItemLoad())
    }, [dispatch])

    let books = getFromLocalStorage('CartList');
    if (cartItems === null || cartItems[0] === undefined) {
        return (
            <p className='cart__no-items-text'>Oops... No books have been added</p>
        )
    } else {
        books = getFromLocalStorage('CartList').map((el, index) =>

            <CartItem
                title={el.title}
                price={el.price}
                path={el.path}
                vendorCode={el.vendorCode}
                color={el.color}
                item={el}
                key={index}
            />
        )
    }
    return (
        <div className='cart'>
            {books}
        </div>

    );
};

export default Cart;   