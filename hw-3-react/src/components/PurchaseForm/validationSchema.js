import * as yup from 'yup';

const purchaseSchema = yup.object().shape({
    userName: yup
        .string()
        .required('this field is required'),
    userLastName: yup
        .string()
        .required('this field is required'),
    userAge: yup
        .number()
        .required('how old are you, pal?')
        .min(5, 'are you able to read, kid ?'),
    shippingAddress: yup
        .string()
        .required('where to send ?'),
    userPhoneNumber: yup
        .number()
        .required('do not worry, we will not bother you')
})

export default purchaseSchema;