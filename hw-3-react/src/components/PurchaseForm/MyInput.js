import React from 'react';
import {ErrorMessage} from "formik";

const MyInput = ({form, field, ...rest}) => {
    const {name} = field;

    return (
        <>
            <input {...field} {...rest}/>
            {<ErrorMessage name={name}/>}
        </>
    );
};

export default MyInput;