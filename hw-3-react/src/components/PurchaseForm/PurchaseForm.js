import React, {useState} from 'react';
import {ErrorMessage, Formik, Form, Field} from "formik";
import purchaseSchema from "./validationSchema";
import './PurchseForm.scss'
import {useDispatch, useSelector} from "react-redux";
import {cleanOutCart} from "../../redux/cart/cartActions";

const PurchaseForm = () => {
    const dispatch = useDispatch()
    const cart = useSelector(state => {
         return state.cart
    })
    const [form, setForm] = useState(true)

    return (
        form?
        <>
            <Formik
                initialValues={{
                    userName: '',
                    userLastName: '',
                    userAge: '',
                    shippingAddress: '',
                    userPhoneNumber: '',
                }}
                onSubmit={(values, {setSubmitting}) => {
                    setTimeout(() => {
                        console.log(`Customer: ${JSON.stringify(values)}\nBooks to buy: ${JSON.stringify(cart)}`);
                        dispatch(cleanOutCart())
                        setSubmitting(false);
                        setForm(false)
                    }, 1000)
                }}
                validationSchema={purchaseSchema}
            >{() => {
                return <Form noValidate className='purchaseForm'>
                    <Field className='userName'
                           component='input'
                           type='text'
                           name='userName'
                           placeholder='Enter your name'
                    />
                    <ErrorMessage name='userName'/>

                    <Field className='userLastName'
                           component='input'
                           type='text'
                           name='userLastName'
                           placeholder='Enter your lastname'
                    />
                    <ErrorMessage name='userLastName'/>

                    <Field className='userAge'
                           component='input'
                           type='number'
                           name='userAge'
                           placeholder='Enter your age'
                    />
                    <ErrorMessage name='userAge'/>

                    <Field className='shippingAddress'
                           component='input'
                           type='text'
                           name='shippingAddress'
                           placeholder='Enter your shipping address'
                    />
                    <ErrorMessage name='shippingAddress'/>

                    <Field className='userPhoneNumber'
                           component='input'
                           type='number'
                           name='userPhoneNumber'
                           placeholder='Enter your phone number'
                    />
                    <ErrorMessage name='userPhoneNumber'/>
                    <button className='purchaseForm__btn'
                            type='submit'
                            >
                        Checkout
                    </button>
                </Form>
            }}
            </Formik>
        </>
            : null
    );
};

export default PurchaseForm;