import React from 'react';
import BookTemplate from "../Book/BookTemplate";
import {useDispatch, useSelector} from "react-redux";
import {removeFavoriteFromCart} from "../redux/favorites/favoritesActions";

const FavoriteItem = (props) => {

    const dispatch = useDispatch();
    const removeItem = useSelector(state =>{return state.favorites})

    const {
        title,
        price,
        path,
        vendorCode,
        color,
        item
    } = props;

    return (
        <>
            {removeItem ?
                <BookTemplate
                    header={<span className={"book-container__favorites-star"}
                                  onClick={() => dispatch (removeFavoriteFromCart(item))}>
                            <i className="fas fa-star"/>
                                        </span>}
                    title={title}
                    vendorCode={vendorCode}
                    color={color}
                    path={path}
                    price={price}
                />
                : null}
        </>
    );
};

export default FavoriteItem;