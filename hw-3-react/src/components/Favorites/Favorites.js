import React, {useEffect} from 'react';
import './Favorites.scss';
import FavoriteItem from '../Favorites/FavoriteItem'
import {loadFavoriteToCart} from "../redux/favorites/favoritesActions";
import {useDispatch, useSelector} from "react-redux";
import {getFromLocalStorage} from "../utils";


const Favorites = () => {

    const dispatch = useDispatch();
    const loadFavorites = useSelector(state => {
        return state.favorites.favorites
    });
    useEffect(() => {
        dispatch(loadFavoriteToCart())
    }, [dispatch])

    let books = getFromLocalStorage('FavoriteItemsList');
    if (loadFavorites === null || loadFavorites[0] === undefined) {
        return (
            <p className='cart__no-items-text'>Oops... No books have been added</p>
        )
    } else {
        books = getFromLocalStorage('FavoriteItemsList').map((el, index) =>
            <FavoriteItem
                title={el.title}
                price={el.price}
                path={el.path}
                vendorCode={el.vendorCode}
                color={el.color}
                item={el}
                key={index}
            />)
    }
    return (
        <div className='favorites'>
            {books}
        </div>
    );
};

export default Favorites;