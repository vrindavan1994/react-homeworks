import React from 'react';

const BookTemplate = (props) => {
    const {
        title,
        price,
        path,
        vendorCode,
        color,
        footer,
        header,
        deleteButton,

    } = props;

    return (
        <div className='book-container'>
            {deleteButton}
            {header}
            <img className="book-container__image" src={path} alt="book logo"/>
            <p className="book-container__title">{title}</p>
            <p className="book-container__price">Price: {price}$</p>
            <p className="book-container__vendor-code">code: {vendorCode}</p>
            <div className="book-container__color"><p>color:</p>
                <div className="book-container__color-form" style={{backgroundColor: color}}></div>
            </div>
            {footer}
        </div>
    );
};

export default BookTemplate;