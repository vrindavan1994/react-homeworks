import React, {useEffect, useState} from 'react';
import "./Book.scss"
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";
import BookTemplate from "./BookTemplate";
import {useDispatch, useSelector} from "react-redux";
import {hideCartModal, showAddCartModal} from "../redux/modal/modalActions";
import {addFavoriteToCart, removeFavoriteFromCart} from "../redux/favorites/favoritesActions";
import {addItemToCart} from "../redux/cart/cartActions";

const Book = props => {

    const dispatch = useDispatch()
    const modalState = useSelector(state => {
        return state.modal.isAddToCart
    });

    const [favoriteItem, setFavoriteItem] = useState(false)
    const {
        title,
        price,
        path,
        vendorCode,
        color,
        item
    } = props;

    useEffect(() => {
        if (localStorage.getItem(`favoriteItem ${vendorCode}`)) {
            setFavoriteItem(true)
        }
    }, [setFavoriteItem, vendorCode])

    return (
        <>
            <BookTemplate
                title={title}
                price={price}
                path={path}
                color={color}
                vendorCode={vendorCode}
                header={favoriteItem || localStorage.getItem(`favoriteItem ${vendorCode}`)
                    ?
                    <span className={"book-container__favorites-star"}
                          onClick={() => {
                              removeFavoriteFromCart(item);
                              setFavoriteItem(false)
                          }}>
        <i className="fas fa-star"/></span>
                    :
                    <span className={"book-container__favorites"}
                          onClick={() => {
                              addFavoriteToCart(item);
                              setFavoriteItem(true)
                          }}>
        <i className="far fa-star"/></span>}
                footer={<button className="book-container__add-to-cart-btn"
                                onClick={() => {
                                    dispatch(showAddCartModal())
                                    dispatch(addItemToCart(item))
                                }}>
                    Add to cart
                </button>}
            />

            {modalState ?
                <Modal
                    header="Excellent choice, pal!"
                    text="Do you wanna add this book to your cart?"
                    closeButton={() => dispatch(hideCartModal())}
                    actions={
                        <>
                            <button onClick={() => dispatch(hideCartModal())}
                                    className="modal__window-footer-btn-cancel">CANCEL
                            </button>
                            <button onClick={() => {dispatch(hideCartModal())}}
                                    className="modal__window-footer-btn-ok">ADD
                                TO CART
                            </button>
                        </>
                    } onBlur={(event) => event.currentTarget === event.target && hideCartModal()}
                /> : null
            }
        </>
    );
}


export default Book;

Book.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    favorite: PropTypes.bool
}