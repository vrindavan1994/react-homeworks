import React, {useEffect} from 'react';
import Book from "../Book/Book";
import "./MainPage.scss"
import PropTypes from 'prop-types'
import {useDispatch, useSelector} from 'react-redux'
import {fetchBooks} from "../redux/books/booksActions";

const MainPage = () => {

    const dispatch = useDispatch()
    const fetchedBooks = useSelector(state => {
        return state.books.all
    })


    useEffect(() => dispatch(fetchBooks()), [dispatch])


    const books = fetchedBooks.map((el, index) =>
        <Book
            title={el.title}
            price={el.price}
            path={el.path}
            vendorCode={el.vendorCode}
            color={el.color}
            key={index}
            item={el}
        />)

    return (
        <div className="main-page">
            {books}
        </div>
    );

};


export default MainPage;

MainPage.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    key: PropTypes.number,
    items: PropTypes.array
}