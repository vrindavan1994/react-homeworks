import React from 'react';
import './Modal.scss';

const Modal = props => {

    const {
        header,
        text,
        closeButton,
        actions,
        onBlur
    } = props
    return (
        <div className="modal" onClick={onBlur}>
            <div className="modal__window">
                <p className="modal__window-header">
                    {header}
                    <button onClick={closeButton} className="modal__window-header__close-btn">X</button>
                </p>
                <p className="modal__window-body">
                    {text}
                </p>
                <div className="modal__window-footer">
                    {actions}
                </div>
            </div>
        </div>
    );

}

export default Modal;