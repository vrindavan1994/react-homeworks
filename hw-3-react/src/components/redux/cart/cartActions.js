import {ADD_TO_CART, LOAD_TO_CART, REMOVE_FROM_CART} from "../actionTypes";
import {getFromLocalStorage, setToLocalStorage} from "../../utils";

export const cartItemLoad = () => dispatch => {
    const cartList = getFromLocalStorage('CartList', []);
    dispatch({type: LOAD_TO_CART, payload: cartList});
}

export const addItemToCart = item => dispatch => {
    const cartList = getFromLocalStorage('CartList', []);
    cartList.push(item);
    setToLocalStorage('CartList', cartList);
    dispatch({type: ADD_TO_CART, payload: cartList});
}

export const removeCartItem = item => dispatch => {
    let cartList = getFromLocalStorage('CartList', []);
    cartList = cartList.filter(book => book.vendorCode !== item.vendorCode);
    setToLocalStorage('CartList', cartList);
    dispatch({type: REMOVE_FROM_CART, payload: cartList});
}
