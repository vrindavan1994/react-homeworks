import {ADD_TO_CART} from "../actionTypes";
import {LOAD_TO_CART} from "../actionTypes";
import {REMOVE_FROM_CART} from "../actionTypes";

const initialState = {
    cart: []
}

export const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_TO_CART:
            return {...state, cart: action.payload}
        case ADD_TO_CART:
            return {...state, cart: action.payload}
        case REMOVE_FROM_CART:
            return {...state, cart: action.payload}
        default:
            return state;
    }

}
