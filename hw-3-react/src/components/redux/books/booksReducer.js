import {FETCH_BOOKS} from "../actionTypes";


const initialState = {
    all: []
}

export const booksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_BOOKS:
            return {...state, all: action.payload}
        default:
            return state

    }
}