import {ADD_TO_FAVORITES, REMOVE_FROM_FAVORITES, LOAD_TO_FAVORITES} from '../actionTypes'
import {getFromLocalStorage, setToLocalStorage} from "../../utils";

export const loadFavoriteToCart = () => dispatch => {
    const favArr = getFromLocalStorage('FavoriteItemsList', [])
    dispatch({
        type: LOAD_TO_FAVORITES,
        payload: favArr
    })
}

export function addFavoriteToCart(item) {
    setToLocalStorage(`favoriteItem ${item.vendorCode}`, item)
    let favBooks = getFromLocalStorage('FavoriteItemsList');
    if (favBooks == null) favBooks = [];
    favBooks.push(item);
    let book = Array.from(new Set(favBooks.map(JSON.stringify))).map(JSON.parse);
    setToLocalStorage('FavoriteItemsList', book);
    return dispatch => dispatch({type: ADD_TO_FAVORITES, payload: book})
}

export function removeFavoriteFromCart(item) {

    let favArr = JSON.parse(localStorage.getItem('FavoriteItemsList'));
    localStorage.removeItem(`favoriteItem ${item.vendorCode}`)
    favArr = favArr.filter(book => book.vendorCode !== item.vendorCode);
    setToLocalStorage('FavoriteItemsList', favArr);
    return dispatch => dispatch({type: REMOVE_FROM_FAVORITES, payload: favArr})
}