import {ADD_TO_FAVORITES, REMOVE_FROM_FAVORITES, LOAD_TO_FAVORITES} from "../actionTypes";

const initialState = {
    favorites: []
}

export const favoritesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_FAVORITES:
            return {...state, favorites: action.payload}
        case LOAD_TO_FAVORITES:
            return {...state, favorites: action.payload}
        case REMOVE_FROM_FAVORITES:
            return {...state, favorites: action.payload}
        default:
            return state
    }
}