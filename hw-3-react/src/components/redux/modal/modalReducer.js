import {SHOW_CART_ADD_MODAL, HIDE_MODAL,SHOW_CART_REMOVE_MODAL} from "../actionTypes";

const initialState = {
    isAddToCart: false,
    isDeleteFromCart: false,
}

export const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_CART_ADD_MODAL:
            return {...state, isAddToCart: true}
        case SHOW_CART_REMOVE_MODAL:
            return {...state, isAddToCart: true}
        case HIDE_MODAL:
            return initialState;
        default:
            return state
    }
}