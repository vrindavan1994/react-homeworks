import React, {Component} from 'react';
import Modal from "./components/Modal/Modal";
import './App.scss'
import Header from "./components/Header/Header";
import MainPage from "./components/MainPage/MainPage";
import Footer from "./components/Footer/Footer";
import PropTypes from "prop-types";

class App extends Component {

    state = {
        isModalOpen: false,
    };
    toggleModal = () => {
        this.setState({isModalOpen: !this.state.isModalOpen})
    }


    render() {
        return (
            <div className="app">
                <Header
                />

                {this.state.isModalOpen ?
                    <Modal
                        header="Excellent choice, pal!"
                        text="Do you wanna add this book to your cart?"
                        closeButton={this.toggleModal}
                        actions={
                            <>
                                <button onClick={this.toggleModal}
                                        className="modal__window-footer-btn-cancel">CANCEL
                                </button>
                                <button onClick={this.toggleModal} className="modal__window-footer-btn-ok">ADD TO CART
                                </button>
                            </>
                        } onBlur={(event) => event.currentTarget === event.target && this.toggleModal()}
                    /> : null
                }
                <MainPage onClick={this.toggleModal}/>
                <Footer/>
            </div>
        );
    }
}

export default App;

App.propTypes = {
    isModalOpen: PropTypes.bool,
    toggleModal: PropTypes.func,
}
