import React, {Component} from 'react';
import "./Footer.scss"

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <p>Copyright &copy; 2020 <span className="footer__logo-you">You</span>
                    <span className="footer__logo-book">Book</span> store
                </p>
            </div>
        );
    }
}

export default Footer;