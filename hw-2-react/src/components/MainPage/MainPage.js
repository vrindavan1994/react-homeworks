import React, {Component} from 'react';
import Book from "../Book/Book";
import "./MainPage.scss"
import PropTypes from 'prop-types'

class MainPage extends Component {
    state = {
        items: []
    }

    async componentDidMount() {
        await fetch("./books-storage.json")
            .then(r => r.json())
            .then(data => {
                this.setState({items: data})
            })
    }

    render() {
        const {onClick} = this.props
        const {items} = this.state
        const books = items.map((el, index) =>
            <Book
                title={el.title}
                price={el.price}
                path={el.path}
                vendorCode={el.vendorCode}
                color={el.color}
                key={index}
                onClick={onClick}
            />)


        return (
            <>
                <div className="main-page">
                    {books}
                </div>
            </>
        );
    }
}

export default MainPage;

MainPage.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    key: PropTypes.number,
    items: PropTypes.array
}