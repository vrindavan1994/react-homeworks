import React, {Component} from 'react';
import "./Book.scss"
import PropTypes from "prop-types";

class Book extends Component {

    state = {
        favoriteItem: false
    }

    handleAddFavoriteItem = () => {
        localStorage.setItem(`favoriteItem ${this.props.vendorCode}`, JSON.stringify(this.props))
        this.setState({favoriteItem: true})
    }
    handleDeleteFavoriteItem = () => {
        localStorage.removeItem(`favoriteItem ${this.props.vendorCode}`)
        this.setState({favoriteItem: false})
    }
    handleAddItemToCart = event => {
        if (event.target.classList.contains("book-container__add-to-cart-btn")) {
            localStorage.setItem(`chosenItem ${this.props.vendorCode}`, JSON.stringify(this.props))
            this.props.onClick()
        }
    }

    render() {
        const {
            title,
            price,
            path,
            vendorCode,
            color
        } = this.props;
        const {favoriteItem} = this.state

        return (
            <div className="book-container">
                {favoriteItem || localStorage.getItem(`favoriteItem ${this.props.vendorCode}`)
                    ?
                    <span className={"book-container__favorites-star"}
                          onClick={this.handleDeleteFavoriteItem}>
        <i className="fas fa-star"/></span>
                    :
                    <span className={"book-container__favorites"}
                          onClick={this.handleAddFavoriteItem}>
        <i className="far fa-star"/></span>}

                <img className="book-container__image" src={path} alt="book logo"/>
                <p className="book-container__title">{title}</p>
                <p className="book-container__price">Price: {price}$</p>
                <p className="book-container__vendor-code">code: {vendorCode}</p>
                <div className="book-container__color"><p>color:</p>
                    <div className="book-container__color-form" style={{backgroundColor: color}}></div>
                </div>


                <button className="book-container__add-to-cart-btn" onClick={this.handleAddItemToCart}>
                    Add to cart
                </button>
            </div>
        );
    }
}

export default Book;

Book.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    favorite: PropTypes.bool
}