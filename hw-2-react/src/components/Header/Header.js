import React, {Component} from 'react';
import "./Heaader.scss"

class Header extends Component {
    render() {
        return (
            <header className="header">
                <h2 className="header__logo">You
                    <span className="header__logo-span">Book</span>
                </h2>
                <p className="header__cart">
                    <i className="fas fa-star"></i>
                    <span className="header__cart-text">Favorites</span>
                    <i className="fas fa-cart-plus"></i>
                    <span className="header__cart-text">Your cart</span>
                </p>
            </header>
        );
    }
}

export default Header;