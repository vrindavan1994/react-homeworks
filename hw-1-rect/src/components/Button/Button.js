import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    render() {
        const {
            text,
            onClick,
            backgroundColor
        } = this.props
        return (
            <button className="buttons" style={{backgroundColor: backgroundColor}} onClick={onClick}>{text}</button>
        );
    }
}

export default Button;