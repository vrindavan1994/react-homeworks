import React, {Component} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false
  };
  toggleFirstModal = () => {
    this.setState({isFirstModalOpen: !this.state.isFirstModalOpen})
  }
  toggleSecondModal = () => {
    this.setState({isSecondModalOpen: !this.state.isSecondModalOpen})
  }

  render() {
    return (
        <div>
          <Button
              text="Open first modal"
              onClick={this.toggleFirstModal}
              backgroundColor={"darkred"}
          />
          <Button
              text="Open second modal"
              onClick={this.toggleSecondModal}
              backgroundColor={"darkblue"}
          />


          {this.state.isFirstModalOpen ?
              <Modal
                  header="Do you want to delete this file?"
                  text="First modal"
                  closeButton={this.toggleFirstModal}
                  actions={
                    <>
                      <button onClick={this.toggleFirstModal}
                              className="modal__window-footer-btn-cancel">Cancel
                      </button>
                      <button className="modal__window-footer-btn-ok">Ok</button>
                    </>
                  } onBlur={(event) => event.currentTarget === event.target && this.toggleFirstModal()}
              /> : null
          }
          {this.state.isSecondModalOpen ?
              <Modal
                  header="Do you want to delete this file?"
                  text="Second modal"
                  closeButton={this.toggleSecondModal}
                  actions={
                    <>
                      <button onClick={this.toggleSecondModal}
                              className="modal__window-footer-btn-cancel">Cancel
                      </button>
                      <button className="modal__window-footer-btn-ok">Ok</button>
                    </>
                  } onBlur={(event) => event.currentTarget === event.target && this.toggleSecondModal()}
              /> : null
          }

        </div>
    );
  }
}

export default App;
